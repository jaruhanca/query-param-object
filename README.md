# Query Param Object
Demo para enviar lista de objetos en query parameter.

## Ejemplo
* Se enviara la siguiente lista de objetos en formato json:
```json
[
   {
      "eventId":"SPS00000000000000000001",
      "eventTime":"2018-01-20T09:30:47.233+01:00",
      "timeOcurred":"2018-01-20T09:30:42.233+01:00",
      "event":{
         "subscriber":"987410140",
         "createTopUpRequest":{
            "type":"bolton",
            "itemProducts":[
               {
                  "product":{
                     "id":"1351481",
         "productType":"supercharge"
                  },
                  "channel":{
                     "id":"Facebook"
                  },
                  "requestor":{
                     "id":"FB000012AC43B84539AC12"
                  },
                  "amount":{
                     "amount":1.00,
                     "units":"PEN"
                  },
                  "payment":{
                     "id":"af105148-10c4-4ef1-9291-679a65265093",
         "type":"balance"
                  },
                  "requestedDate":"2018-01-20T09:30:47.233+01:00"
               }
            ]
         }
      }
   }
]
```
* El json se tiene que convertir a formato [Url Encoding](https://en.wikipedia.org/wiki/Percent-encoding), puedes usar este [link](https://www.url-encode-decode.com/).

* Utilizamos la siguiente url para probar la demo : 

http://localhost:8080/object-parameter?objects=%5B%7B%22eventId%22%3A%22SPS00000000000000000001%22%2C%22eventTime%22%3A%222018-01-20T09%3A30%3A47.233%2B01%3A00%22%2C%22timeOcurred%22%3A%222018-01-20T09%3A30%3A42.233%2B01%3A00%22%2C%22event%22%3A%7B%22subscriber%22%3A%22987410140%22%2C%22createTopUpRequest%22%3A%7B%22type%22%3A%22bolton%22%2C%22itemProducts%22%3A%5B%7B%22product%22%3A%7B%22id%22%3A%221351481%22%2C%22productType%22%3A%22supercharge%22%7D%2C%22channel%22%3A%7B%22id%22%3A%22Facebook%22%7D%2C%22requestor%22%3A%7B%22id%22%3A%22FB000012AC43B84539AC12%22%7D%2C%22amount%22%3A%7B%22amount%22%3A1.00%2C%22units%22%3A%22PEN%22%7D%2C%22payment%22%3A%7B%22id%22%3A%22af105148-10c4-4ef1-9291-679a65265093%22%2C%22type%22%3A%22balance%22%7D%2C%22requestedDate%22%3A%222018-01-20T09%3A30%3A47.233%2B01%3A00%22%7D%5D%7D%7D%7D%5D

## Recursos
* https://www.baeldung.com/spring-type-conversions
* https://stackoverflow.com/questions/44589381/how-to-convert-json-string-into-list-of-java-object


## Observaciones
* Longitud maxima de una url (https://stackoverflow.com/questions/812925/what-is-the-maximum-possible-length-of-a-query-string).
* Configuración de longitud de url en spring boot (https://stackoverflow.com/questions/39720422/java-tomcat-request-header-is-too-large).
