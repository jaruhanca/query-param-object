package com.telefonica.parameter.pojo;

import lombok.Data;

@Data
public class Money {

	private Double amount;

	private String units;
}
