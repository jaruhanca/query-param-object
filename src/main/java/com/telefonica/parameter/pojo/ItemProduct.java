package com.telefonica.parameter.pojo;

import lombok.Data;

@Data
public class ItemProduct {

	private Product product;

	private Channel channel;

	private Requestor requestor;

	private Money amount;

	private Payment payment;

	private String requestedDate;
}
