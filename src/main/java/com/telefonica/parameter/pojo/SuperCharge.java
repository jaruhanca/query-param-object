package com.telefonica.parameter.pojo;

import lombok.Data;

@Data
public class SuperCharge {

	private String eventId;

	private String eventTime;

	private String timeOcurred;

	private Event event;
}
