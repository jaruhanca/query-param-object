package com.telefonica.parameter.pojo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateTopUpRequest {

	private String type;

	List<ItemProduct> itemProducts;
}
