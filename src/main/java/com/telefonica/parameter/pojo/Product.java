package com.telefonica.parameter.pojo;

import lombok.Data;

@Data
public class Product {

	private String id;

	private String productType;
}
