package com.telefonica.parameter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueryParamObjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(QueryParamObjectApplication.class, args);
	}

}
