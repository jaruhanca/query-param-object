package com.telefonica.parameter.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.telefonica.parameter.converter.StringToSuperCharge;

@Configuration
public class WebConfig implements WebMvcConfigurer{
	
	 @Override
	 public void addFormatters(FormatterRegistry registry) {
	        registry.addConverter(new StringToSuperCharge());
	 }
}
